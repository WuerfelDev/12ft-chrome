function validURL(u){
    let url = new URL(u)
    return url.protocol === "http:" || url.protocol === "https:"
}

function resetAction(tabId){
    chrome.action.enable(tabId)
    chrome.action.setTitle({tabId:tabId,title:"Unblock this site"})
    chrome.action.setBadgeText({tabId:tabId,text:""})
}

function badgeAdd(tabId){
    chrome.action.setTitle({tabId:tabId,title:"Redirect this website automatically"})
    chrome.action.setBadgeText({tabId:tabId,text:"+"})
    chrome.action.setBadgeBackgroundColor({tabId:tabId,color:"#00AA00"})
}

function badgeRemove(tabId){
    chrome.action.setTitle({tabId:tabId,title:"Remove the automatic redirect"})
    chrome.action.setBadgeText({tabId:tabId,text:"-"})
    chrome.action.setBadgeBackgroundColor({tabId:tabId,color:"#FF0000"})
}


// New page is loading
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if(changeInfo.status == "loading" && "url" in changeInfo){
        resetAction(tabId)
        if(validURL(changeInfo.url)){
            let currenturl = new URL(changeInfo.url)
            if(currenturl.hostname == "12ft.io"){
                if(currenturl.searchParams.has('q')){
                    let domain = new URL(currenturl.searchParams.get('q')).hostname
                    chrome.storage.local.get('redirects', function(result) {
                        if(result['redirects'] && result['redirects'].includes(domain)){
                            badgeRemove(tabId)
                        }else{
                            badgeAdd(tabId)
                        }
                    })
                }else{
                    // 12ft has no q parameter.
                    chrome.action.disable(tabId)
                }
            }else{
            
                chrome.storage.local.get('redirects', function(result) {
                    if(result['redirects'] && result['redirects'].includes(currenturl.hostname)){
                        // Automatic redirect
                        chrome.tabs.update(tabId, {url: "https://12ft.io/"+changeInfo.url})
                    }
                })
            }
        }else{
            // Wrong protocol, disable icon
            chrome.action.disable(tabId)
        }
    }
})


// icon clicked
chrome.action.onClicked.addListener((tab) => {
    if(validURL(tab.url)){
        let currenturl = new URL(tab.url)
        if(currenturl.hostname == "12ft.io" && currenturl.searchParams.has('q')){
            let domain = new URL(currenturl.searchParams.get('q')).hostname
            chrome.storage.local.get('redirects', function(result) {
                let newval = 'redirects' in result? result['redirects']:[]
                if(newval.includes(domain)){
                    // Remove url from redirects
                    newval = newval.filter(x => x !== domain)
                    badgeAdd(tab.id)
                }else{
                    // Add url to redirects
                    newval.push(domain)
                    badgeRemove(tab.id)
                }
                chrome.storage.local.set({'redirects': newval})
            })
        }else{
            chrome.tabs.update(tab.id, {url: "https://12ft.io/"+tab.url})
        }
    }
})
